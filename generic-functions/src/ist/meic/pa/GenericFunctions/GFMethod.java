package ist.meic.pa.GenericFunctions;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class GFMethod {
	
	// return the methods defined in this object
	public Method[] getDeclaredMethods(){
		Class<?> gfClass = this.getClass();
		Method[] methods = gfClass.getDeclaredMethods();
		return methods;
	}
	
	// returns the method 'call' if it exists
	public Method getCallMethod(){
		return Arrays.stream(getDeclaredMethods())
					 .filter(method -> method.getName().equals("call"))
					 .findAny()
					 .orElse(null);
	}
	
	public boolean hasCallMethod(){
		return (getCallMethod() != null) ? true : false;
	}
	
	public Object invokeCall(Object... args){
		try {
			Method callMethod = getCallMethod();
			callMethod.setAccessible(true);
			return callMethod.invoke(this, args);
		} catch (InvocationTargetException | IllegalAccessException e) {
			if (e.getCause() instanceof IllegalArgumentException){
				throw (IllegalArgumentException) e.getCause();
			}
		}
		return null;
	}

	// returns the number of args of the 'call' method
	public int getArgsLength() {
		return getCallMethod().getParameterCount();
	}
	
	public Class<?>[] getParameterTypes(){
		return this.getCallMethod().getParameterTypes();
	}

}
