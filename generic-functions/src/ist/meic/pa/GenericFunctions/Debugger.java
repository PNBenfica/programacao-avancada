package ist.meic.pa.GenericFunctions;

import java.util.Arrays;

public class Debugger {
	public static boolean debugger = false;
	
	public static void log(String... messages){	
		if(debugger){
			System.out.print("debug---|");
			Arrays.stream(messages).forEach(message -> System.out.print(" " + message + " |"));
		System.out.println("---debug");
		}
	}
	
}
