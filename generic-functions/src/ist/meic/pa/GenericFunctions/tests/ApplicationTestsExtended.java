package ist.meic.pa.GenericFunctions.tests;

import ist.meic.pa.GenericFunctions.Debugger;
import ist.meic.pa.GenericFunctionsExtended.GFMethodExtended;
import ist.meic.pa.GenericFunctionsExtended.GenericFunctionsExtended;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;

public class ApplicationTestsExtended {

	@Test
	public void addTwoIntegers() {
		final GenericFunctionsExtended add = new GenericFunctionsExtended("add");
		add.addMethod(new GFMethodExtended() {
			Object call(Double a, Integer b, Object c) {
				return "Merda2";
			}

		});
		add.addMethod(new GFMethodExtended() {
			Object call(Double a, Integer b, Integer c) {
				return "Porreiro";
			}

		});
		add.addMethod(new GFMethodExtended() {
			Object call(Object a, Object b, Integer c) {
				return "Merda";
			}

		});

		Object response = add.call(1.0, 3, 4);
//		System.out.println(response);
		assertEquals("Porreiro", response);
	}

	@Test
	public void singleArgTest() {
		final GenericFunctionsExtended add = new GenericFunctionsExtended("add");
		add.addMethod(new GFMethodExtended() {
			Object call(Integer a) {
				return a;
			}

		});
		add.addMethod(new GFMethodExtended() {
			Object call(Object[] a) {
				Object[] r = new Object[a.length];
				return r;
			}
		});

		Object response = add.call(3);
//		System.out.println(response);
		assertEquals(3, response);
	}

	@Test
	public void testWithTwoClasses() {
		final GenericFunctionsExtended cao = new GenericFunctionsExtended("cao");
		cao.addMethod(new GFMethodExtended() {
			Object call(Cao a) {
				return a;
			}

		});
		cao.addMethod(new GFMethodExtended() {
			Object call(Animal a) {
				return a;
			}

		});

		Object response = cao.call(new Cao());
//		System.out.println(response);
		assertEquals("Sou um cão chamado Ruben", response.toString());

		response = cao.call(cao.call(new Animal()));
//		System.out.println(response);
		assertEquals("Sou um Animal chamado Rubén", response.toString());

	}

	
	@Test
	public void testAddGiven() {
		final GenericFunctionsExtended add = new GenericFunctionsExtended("add");
		add.addMethod(new GFMethodExtended() {
			Object call(Integer a, Integer b) {
				Debugger.log("Integer, Integer");
				return a + b;
			}
		});
		add.addMethod(new GFMethodExtended() {
			Object call(Object[] a, Object[] b) {
				Debugger.log("Object[], Object[]");
				Object[] r = new Object[a.length];
				for (int i = 0; i < a.length; i++) {
					r[i] = add.call(a[i], b[i]);
				}
				return r;
			}
		});

		add.addMethod(new GFMethodExtended() {
			Object call(Object[] a, Object b) {
				Debugger.log("Object[], Object");
				Object[] ba = new Object[a.length];
				Arrays.fill(ba, b);
				return add.call(a, ba);
			}
		});
		add.addMethod(new GFMethodExtended() {
			Object call(Object a, Object b[]) {
				Debugger.log("Object, Object[]");
				Object[] aa = new Object[b.length];
				Arrays.fill(aa, a);
				return add.call(aa, b);
			}
		});

		

		add.addMethod(new GFMethodExtended() {
			Object call(String a, Object b) {
				Debugger.log("String, Object");
				return add.call(Integer.decode(a), b);
			}
		});
		add.addMethod(new GFMethodExtended() {
			Object call(Object a, String b) {
				Debugger.log("Object , String");
				return add.call(a, Integer.decode(b));
			}
		});
		add.addMethod(new GFMethodExtended() {
			Object call(Object[] a, List b) {
				Debugger.log("Object[], List");
				return add.call(a, b.toArray());
			}
		});
//		println(add.call(new Object[] { 1, 2 }, 3));
//		println(add.call(1, new Object[][] { { 1, 2 }, { 3, 4 } }));
//		println(add.call("12", "34"));
//		println(add.call(new Object[] { "123", "4" }, 5));
//		println(add.call(new Object[] { 1, 2, 3 }, Arrays.asList(4, 5, 6)));


		assertTrue(Arrays.equals(new Integer[]{4,5}, (Object[]) add.call(new Object[] { 1, 2 }, 3)));
		assertTrue(Arrays.deepEquals(new Integer[][]{new Integer[]{2,3} ,new Integer[]{4,5}}, (Object[]) (add.call(1, new Object[][] { { 1, 2 }, { 3, 4 } }))));
		assertEquals(46, add.call("12", "34"));
		assertTrue(Arrays.equals(new Integer[]{128,9}, (Object[]) add.call(new Object[] { "123", "4" }, 5)));
		assertTrue(Arrays.equals(new Integer[]{5,7,9}, (Object[]) add.call(new Object[] { 1, 2, 3 }, Arrays.asList(4, 5, 6))));

	}

	@Test
	public void explainTest() {
		final GenericFunctionsExtended explain = new GenericFunctionsExtended("explain");
		explain.addMethod(new GFMethodExtended() {
			Object call(Integer entity) {
				System.out.printf("%s is a integer", entity);
				return "";
			}
		});
		explain.addMethod(new GFMethodExtended() {
			Object call(Number entity) {
				System.out.printf("%s is a number", entity);
				return "";
			}
		});
		explain.addMethod(new GFMethodExtended() {
			Object call(String entity) {
				System.out.printf("%s is a string", entity);
				return "";
			}
		});
		explain.addAfterMethod(new GFMethodExtended() {
			void call(Integer entity) {
				System.out.printf(" (in hexadecimal, is %x)", entity);
			}
		});
		explain.addBeforeMethod(new GFMethodExtended() {
			void call(Number entity) {
				System.out.printf("The number ", entity);
			}
		});
		
		println(explain.call(123));
		println(explain.call("Hi"));
		println(explain.call(3.14159));
	}
	
	@Test
	public void testWithNoPrimaryMethodMatch(){
		final GenericFunctionsExtended add = new GenericFunctionsExtended("add");
		add.addMethod(new GFMethodExtended() {
			Object call(Integer a, Integer b) {
				return a + b;
			}
		});
		add.addMethod(new GFMethodExtended() {
			Object call(Object[] a, Object[] b) {
				Object[] r = new Object[a.length];
				for (int i = 0; i < a.length; i++) {
					r[i] = add.call(a[i], b[i]);
				}
				return r;
			}
		});
		try{
			add.call(new Object[] { 1, 2 }, 3);
			fail();
		}
		catch(IllegalArgumentException e){
//			System.out.println(e);
		}
	}
	
	@Test
	public void testWithNoCallMethod(){
		final GenericFunctionsExtended add = new GenericFunctionsExtended("add");
		add.addMethod(new GFMethodExtended() {
			Object Ola(Integer a, Integer b) {
				return a + b;
			}
		});
		
		try{
			println(add.call(3, 25));
			fail();
		}
		catch(IllegalArgumentException e){
			System.out.println(e);
		}
	}
	

	@Test
	public void testInterfaces1(){
		final GenericFunctionsExtended interfaceTest = new GenericFunctionsExtended("interface");
		interfaceTest.addMethod(new GFMethodExtended() {
			Object call(Interface1 obj) {
				return "Interface1";
			}
		});
		
		assertEquals("Interface1", interfaceTest.call(new Cao()));
		assertEquals("Interface1", interfaceTest.call(new Animal()));
		
		
		interfaceTest.addMethod(new GFMethodExtended() {
			Object call(Interface2 obj) {
				return "Interface2";
			}
		});
		
		assertEquals("Interface2", interfaceTest.call(new Cao()));
		assertEquals("Interface1", interfaceTest.call(new Animal()));
		
		interfaceTest.addMethod(new GFMethodExtended() {
			Object call(Animal obj) {
				return "Animal";
			}
		});
		
		assertEquals("Animal", interfaceTest.call(new Cao()));
		assertEquals("Animal", interfaceTest.call(new Animal()));
	}

	@Test
	public void testArrays1(){
		final GenericFunctionsExtended arrayTest = new GenericFunctionsExtended("interface");
		arrayTest.addMethod(new GFMethodExtended() {
			Object call(Integer obj) {
				return obj;
			}
		});
		arrayTest.addMethod(new GFMethodExtended() {
			Object call(Integer a, Integer b) {
				return 0;
			}
		});
		try{
			arrayTest.call(1, 2);
			fail();
		}
		catch(IllegalArgumentException e){
			System.out.println(e);;
		}
	}

//	@Test
//	public void testArrays2(){
//		final GenericFunctionsExtended interfaceTest = new GenericFunctionsExtended("interface");
//		interfaceTest.addMethod(new GFMethodExtended() {
//			Object call(Animal obj) {
//				return "Animal";
//			}
//		});
//		interfaceTest.addMethod(new GFMethodExtended() {
//			Object call(Animal[] array) {
//				return Arrays.stream(array).map(obj -> interfaceTest.call(obj)).collect(Collectors.toList());
//			}
//		});
//		interfaceTest.addMethod(new GFMethodExtended() {
//			Object call(Animal[] array, Animal[] array2) {
//				return "" + interfaceTest.call(array) + interfaceTest.call(array2);
//			}
//		});
//		println(interfaceTest.call(new Animal[]{new Cao(), new Animal()}, new Animal[]{new Cao(), new Animal()}));
//	}
	
	
	@Test
	public void testRedefinePrimaryMethod(){
		final GenericFunctionsExtended add = new GenericFunctionsExtended("add");
		add.addMethod(new GFMethodExtended() {
			Object call(Double a, Integer b, Object c) {
				return "Merda1";
			}

		});
		add.addMethod(new GFMethodExtended() {
			Object call(Double a, Integer b, Object c){
				return "Porreiro";
			}

		});
		
		Object response = add.call(1.0, 3, 4);
//		System.out.println(response);
		assertEquals("Porreiro", response);
		
	}
	
	@Test
	public void UseCache(){
		final GenericFunctionsExtended add = new GenericFunctionsExtended("add");
		add.addMethod(new GFMethodExtended() {
			Object call(Double a, Integer b, Object c) {
				return "Merda1";
			}

		});
	}


	@Test
	public void testAroundMethod() {
		final GenericFunctionsExtended add = new GenericFunctionsExtended("add");
		add.addMethod(new GFMethodExtended() {
			Object call(Integer a) {
				System.out.println("" + a);
				return a;
			}
		});
		add.addAroundMethod(new GFMethodExtended() {
			Object call(Integer a) {
				System.out.println("Ola,");
				callNextMethod();
				System.out.println("Sanches");
				return "";
			}
		});
		add.addAroundMethod(new GFMethodExtended() {
			Object call(Object a) {
				System.out.println("tudo bem?");
				callNextMethod();
				System.out.println("Renato");
				return "";
			}
		});

		Object response = add.call(35);
		assertEquals(35, response);
	}
	

	@Test
	public void testCallNextMethodInPrimaryMethod() {
		final GenericFunctionsExtended add = new GenericFunctionsExtended("add");
		add.addMethod(new GFMethodExtended() {
			Object call(Integer a) {
				a += (Integer) callNextMethod();
				return a;
			}
		});
		add.addMethod(new GFMethodExtended() {
			Object call(Object a) {
				return 32;
			}
		});

		Object response = add.call(3);
		println(response);
		assertEquals(35, response);
	}
	

	@Test
	public void testCallNextMethodWithDifferentArgs() {
		final GenericFunctionsExtended add = new GenericFunctionsExtended("add");
		add.addMethod(new GFMethodExtended() {
			Object call(Integer a) {
				a += (Integer) callNextMethod(32);
				return a;
			}
		});
		add.addMethod(new GFMethodExtended() {
			Object call(Object a) {
				return a;
			}
		});

		Object response = add.call(3);
		println(response);
		assertEquals(35, response);
	}
	
	@Test
	public void testAroundMethodWithNoCallToNextMethod() {
		final GenericFunctionsExtended add = new GenericFunctionsExtended("add");
		add.addMethod(new GFMethodExtended() {
			Object call(Integer a) {
				return a;
			}
		});
		add.addAroundMethod(new GFMethodExtended() {
			Object call(Integer a) {
				System.out.println("Nao vou chamar mais nenhum metodo");
				return "";
			}
		});

		Object response = add.call(3);
		assertNull(response);
	}
	
	public void println(Object obj) {
		if (obj instanceof Object[]) {
			System.out.println(Arrays.deepToString((Object[]) obj));
		} else {
			System.out.println(obj);
		}
	}

}
