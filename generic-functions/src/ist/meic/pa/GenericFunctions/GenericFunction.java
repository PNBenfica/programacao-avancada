package ist.meic.pa.GenericFunctions;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class GenericFunction {
	private String genericFunctionName;
	
	private List<GFMethod> primaryMethods;
	private List<GFMethod> beforeMethods;
	private List<GFMethod> afterMethods;
	private int argsLength;

	public GenericFunction(String genericFunctionName) {
		this.genericFunctionName = genericFunctionName;
		primaryMethods = new ArrayList<GFMethod>();
		beforeMethods = new ArrayList<GFMethod>();
		afterMethods = new ArrayList<GFMethod>();
	}

	public void addMethod(GFMethod gfMethod) {
		addMethod(primaryMethods, gfMethod);
	}
	
	public void addAfterMethod(GFMethod gfMethod) {
		addMethod(afterMethods, gfMethod);
	}

	public void addBeforeMethod(GFMethod gfMethod) {
		addMethod(beforeMethods, gfMethod);
	}
	
	public void addMethod(List<GFMethod> methods, GFMethod gfMethod){
		if (hasCallMethod(gfMethod) && hasValidArgsLength(gfMethod)){
			if(hasMethodWithSameArgs(methods, gfMethod))
				redefineMethod(methods, gfMethod);
			else
				methods.add(gfMethod);
		}
	}

	// returns true if 'methods' contains any method with the same args of gfmethod
	private boolean hasMethodWithSameArgs(List<GFMethod> methods, GFMethod gfMethod) {
		Class<?>[] argTypes = gfMethod.getParameterTypes();
		return getMethodByArgTypes(methods, argTypes) != null;
	}

	//returns a method that has arguments of type 'argTypes', if exists
	private GFMethod getMethodByArgTypes(List<GFMethod> listMethods, Class<?>[] argTypes) {
		for(GFMethod method: listMethods){
			if(compare(argTypes, method.getParameterTypes()))
				return method;
		}
		return null;
	}
	
	//Change method received by the old one in the given list
	private void redefineMethod(List<GFMethod> listMethods, GFMethod gfMethod) {
		GFMethod oldMethod = getMethodByArgTypes(listMethods, gfMethod.getParameterTypes());
		int index = listMethods.indexOf(oldMethod);
		listMethods.set(index, gfMethod);
	}
	
	//Compares two arrays to verify if the given type parameters are equal
	private boolean compare(Type[] arrayType1, Type[] arrayType2){
		for(int index = 0; index < arrayType1.length; index++){
			if(!arrayType1[index].getTypeName().equals(arrayType2[index].getTypeName())){
				return false;
			}
		}
		return true;
	}
	
	
	// all the methods added must have the same number of arguments
	private boolean hasValidArgsLength(GFMethod gfMethod) {
		if(hasAnyMethod())
			return gfMethod.getArgsLength() == this.argsLength;
		else{
			this.argsLength = gfMethod.getArgsLength();
			return true;
		}
	}

	// returns true if this generic function already has some method
	private boolean hasAnyMethod() {
		return !(primaryMethods.isEmpty() && beforeMethods.isEmpty() && afterMethods.isEmpty());
	}

	private boolean hasCallMethod(GFMethod gfMethod) {
		return gfMethod.hasCallMethod();
	}


	// method that combines and executes the methods to a given set of arguments
	// and returns the response of the primary method
	// if no applicable primary method, a IllegalArgumentException is thrown
	public Object call(Object... args) throws IllegalArgumentException{
		EffectiveMethod effectiveMethod = getEffectiveMethod(args);
		effectiveMethod.invokeMethods(args);
		Object response = effectiveMethod.getResponse();
		return response;
	}
	
	// returns the effective method to the arguments given
	private EffectiveMethod getEffectiveMethod(Object[] args) throws IllegalArgumentException {
		EffectiveMethod effectiveMethod = new EffectiveMethod();
		effectiveMethod.setBeforeMethods(getBeforeMethods(args));
		effectiveMethod.setPrimaryMethod(getPrimaryMethod(args));
		effectiveMethod.setAfterMethods(getAfterMethods(args));
		return effectiveMethod;
	}

	private List<GFMethod> getBeforeMethods(Object[] args) {
		return getApplicableMethods(args, beforeMethods, false);
	}

	private List<GFMethod> getAfterMethods(Object[] args) {
		return getApplicableMethods(args, afterMethods, true);
	}

	private GFMethod getPrimaryMethod(Object[] args) throws IllegalArgumentException{
		List<GFMethod> applicableMethods = getApplicableMethods(args, primaryMethods, false);
		if(applicableMethods.size() > 0)
			return applicableMethods.get(0);
		else{
			throw new IllegalArgumentException(getErrorMessage(args));
		}
	}

	// returns a sorted list of methods eligible to be applied to the given arguments
	// the types of the 'args' must match with the arguments of the 'methods'
	private List<GFMethod> getApplicableMethods(Object[] args, List<GFMethod> methods, boolean reverseOrder) {

		List<MethodPriority> applicableMethods = methods.stream()
														.filter(method-> args.length == method.getCallMethod().getParameterTypes().length)
														.map(MethodPriority::new)
														.collect(Collectors.toList());

		applicableMethods = getApplicableMethods(args, applicableMethods, 0);
		
		if (reverseOrder)
			Collections.reverse(applicableMethods);
		
		return applicableMethods.stream().map(method->method.getGFMethod()).collect(Collectors.toList());
	}

	
	// recursive function that filters and prioritize the applicable methods 
	// based on the argument 'args[index]'
	private List<MethodPriority> getApplicableMethods(Object[] args, List<MethodPriority> applicableMethods, int index) {
		
		try {
			Iterator<MethodPriority> it = applicableMethods.iterator();
			while (it.hasNext()) {
				MethodPriority method = it.next();
			    
				int priority = getMethodPriority(args, method, index);
				if (priority == -1){ // if there is no match the method is removed
					it.remove();
				}
				else{
					method.setPriority(index, priority);
				}
			}
			Collections.sort(applicableMethods);
			
			if (args.length -1 == index){
				return applicableMethods;
			}
			else{
				return getApplicableMethods(args, applicableMethods, ++index);
			}
			
		} catch (Exception e) {
			return null;
		}
	}
	
	// returns the priority of the method 'method' at args[index]
	// return -1 if the method doesn't match args[index]
	private int getMethodPriority(Object[] args, MethodPriority method, int index){
		List<Class<?>> argClass = new ArrayList<Class<?>>();
		argClass.add(args[index].getClass());
		Class<?> methodArgClass = method.getParameterInPosition(index);
		
		return getMethodPriority(argClass, methodArgClass, 0);
	}

	// argParameters - initially has only the current argument being analyzed
	// if matches methodParameter.class returns current level, else goes up one level in the hierarchy
	// and analyzes the arg superclass and implemented interfaces 
	private int getMethodPriority(List<Class<?>> argParameters, Class<?> methodParameter, int priorityLevel){
		int classPriority = priorityLevel;
		int interfacePriority = ++priorityLevel; // interfaces have less precedence than superclass
		List<Class<?>> as = new ArrayList<>();
		if (argParameters.isEmpty()) {
			return -1;
		}
		for (Class<?> parameter : argParameters) {
			if (parameter.equals(methodParameter) || (methodParameter.isArray() && parameter.isArray() && parameter.getComponentType().equals(methodParameter))) {
				return (parameter.isInterface()) ? interfacePriority : classPriority;
			} else {
				Class<?> superClass = getSuperClass(parameter);
				if (superClass != null)
					as.add(superClass);
				as.addAll(Arrays.asList(parameter.getInterfaces()));
			}
		}
		return getMethodPriority(as, methodParameter, ++priorityLevel);
	}

	private Class<?> getSuperClass(Class<?> parameter) {
		Class<?> superClass = null;

		if (parameter.isArray() && parameter.getComponentType() != Object.class) {
			superClass = getArraySuperClass(parameter);
		} else if (parameter != Object.class) {
			superClass = parameter.getSuperclass();
		}
		return superClass;
	}

	private Class<?> getArraySuperClass(Class<?> parameter) {
		try {
			Class<?> componentType = parameter.getComponentType();
			Class<?> clazz = Class.forName("[L" + componentType.getSuperclass().getName() + ";");
			return clazz;
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	
	////// Error Messages

	private String getErrorMessage(Object[] args) {
		String message = "No methods for generic function " + this.genericFunctionName +
						 " with args " + objToString(args) + 
						 " of classes " + getClasses(args);
		return message;
	}
	
	public String objToString(Object obj) {
		if (obj instanceof Object[]) {
			return Arrays.deepToString((Object[]) obj);
		} else {
			return obj.toString();
		}
	}
	
	public String getClasses(Object... obj) {
		String message = "[";
		for(Object o: obj)
			message += o.getClass() + ",";
		if (message.length() > 1)
			message = message.substring(0, message.length() - 1);
		return message + "]";
	}

}
