package ist.meic.pa.GenericFunctions;

import java.util.ArrayList;


public class MethodPriority implements Comparable<MethodPriority>{
	private ArrayList<Integer> argsPriorities = new ArrayList<>();
	private GFMethod gfMethod;

	public MethodPriority(GFMethod gfMethod) {
		this.gfMethod = gfMethod;	
	}
	
	public GFMethod getGFMethod() {
		return gfMethod;
	}
	
	public ArrayList<Integer> getArgsPriority() {
		return argsPriorities;
	}
	
	public void setPriority(int index, int priority){
		if(this.argsPriorities.size() < index){
			this.argsPriorities.set(index, priority);
		}
		else{
			this.argsPriorities.add(index, priority);
		}			
	}
	
	public Class<?> getParameterInPosition(int index) {
		return gfMethod.getParameterTypes()[index];
	}

	public String toString(){
		return "Method: " +gfMethod.getCallMethod()+ " prioritys: "+argsPriorities;
	}

	@Override
	public int compareTo(MethodPriority methodPriority) {
		for(int i = 0; i < this.argsPriorities.size(); i++){
			if(this.argsPriorities.get(i) > methodPriority.getArgsPriority().get(i)){
				return 1;
			}
			if(this.argsPriorities.get(i) < methodPriority.getArgsPriority().get(i)){
				return -1;
			}
		}
		return 0;
	}

}
