package ist.meic.pa.GenericFunctions;

import java.util.List;

public class EffectiveMethod {
	private List<GFMethod> beforeMethods;
	private List<GFMethod> afterMethods;
	private GFMethod primaryMethod;
	private Object response; // holds the response returned by the primary method

	public void invokeMethods(Object... args) {
		invokeBeforeMethods(args);
		this.response = invokePrimaryMethod(args);
		invokeAfterMethods(args);
	}
	
	private void invokeBeforeMethods(Object... args) {
		beforeMethods.forEach(method -> method.invokeCall(args));
	}

	private void invokeAfterMethods(Object... args) {
		afterMethods.forEach(method -> method.invokeCall(args));
	}

	private Object invokePrimaryMethod(Object... args) throws IllegalArgumentException{
		return primaryMethod.invokeCall(args);
	}
	
	public void setBeforeMethods(List<GFMethod> beforeMethods) {
		this.beforeMethods = beforeMethods;
	}

	public void setAfterMethods(List<GFMethod> afterMethods) {
		this.afterMethods = afterMethods;
	}

	public void setPrimaryMethod(GFMethod primaryMethod) {
		this.primaryMethod = primaryMethod;
	}
	
	public Object getResponse() {
		return this.response;
	}
}