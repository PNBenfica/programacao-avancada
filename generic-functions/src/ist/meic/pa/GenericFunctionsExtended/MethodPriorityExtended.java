package ist.meic.pa.GenericFunctionsExtended;

import java.util.ArrayList;


public class MethodPriorityExtended implements Comparable<MethodPriorityExtended>{
	private ArrayList<Integer> argsPriorities = new ArrayList<>();
	private GFMethodExtended gfMethod;

	public MethodPriorityExtended(GFMethodExtended gfMethod) {
		this.gfMethod = gfMethod;	
	}
	
	public GFMethodExtended getGFMethod() {
		return gfMethod;
	}
	
	public ArrayList<Integer> getArgsPriority() {
		return argsPriorities;
	}
	
	public void setPriority(int index, int priority){
		if(this.argsPriorities.size() < index){
			this.argsPriorities.set(index, priority);
		}
		else{
			this.argsPriorities.add(index, priority);
		}			
	}
	
	public Class<?> getParameterInPosition(int index) {
		return gfMethod.getParameterTypes()[index];
	}

	public String toString(){
		return "Method: " +gfMethod.getCallMethod()+ " prioritys: "+argsPriorities;
	}

	@Override
	public int compareTo(MethodPriorityExtended methodPriority) {
		for(int i = 0; i < this.argsPriorities.size(); i++){
			if(this.argsPriorities.get(i) > methodPriority.getArgsPriority().get(i)){
				return 1;
			}
			if(this.argsPriorities.get(i) < methodPriority.getArgsPriority().get(i)){
				return -1;
			}
		}
		return 0;
	}

}
