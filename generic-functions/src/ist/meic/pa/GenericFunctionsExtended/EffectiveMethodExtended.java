package ist.meic.pa.GenericFunctionsExtended;

import java.lang.reflect.Type;
import java.util.List;


public class EffectiveMethodExtended {
	private List<GFMethodExtended> beforeMethods;
	private List<GFMethodExtended> afterMethods;
	private List<GFMethodExtended> aroundMethods;
	private List<GFMethodExtended> primaryMethods;
	private Object response; // holds the response returned by the primary method
	private Object[] args;

	public void invokeMethods(Object... args) {
		this.args = args;
		if (hasAroundMethods()) // around method will then call next method
			invokeAroundMethod(aroundMethods.get(0), this.args);
		else{
			invokeBeforeMethods(this.args);
			this.response = invokePrimaryMethod(this.primaryMethods.get(0), this.args);
			invokeAfterMethods(this.args);
		}
	}

	private void invokeBeforeMethods(Object... args) {
		beforeMethods.forEach(method -> method.invokeCall(args));
	}

	private void invokeAfterMethods(Object... args) {
		afterMethods.forEach(method -> method.invokeCall(args));
	}

	private Object invokeAroundMethod(GFMethodExtended method, Object... args) {
		return method.invokeCall(args);
	}

	private Object invokePrimaryMethod(GFMethodExtended method, Object... args) throws IllegalArgumentException{
		return method.invokeCall(args);
	}
	
	// method called by a gfmethod of this effective method when 'callNextMethod' is invoked 
	public Object callNextMethod(GFMethodExtended method) {
		if(isAroundMethod(method)){
			if(hasNextAroundMethod(method)){
				invokeAroundMethod(getNextAroundMethod(method), this.args);
			}
			else{
				invokeBeforeMethods(this.args);
				this.response = invokePrimaryMethod(this.primaryMethods.get(0), this.args);
				invokeAfterMethods(this.args);
			}
		}
		else if(isPrimaryMethod(method) && hasNextPrimaryMethod(method)){
			return invokePrimaryMethod(getNextPrimaryMethod(method), this.args);
		}
		return null;
	}

	
	// method called by a gfmethod of this effective method when 'callNextMethod' is invoked with arguments
	public Object callNextMethod(GFMethodExtended method, Object[] args) {
		if (validArgs(args)){
			this.args = args;
			return callNextMethod(method);
		}
		return null;
	}
	
	// when a call next method is invoked with new arguments, the same ordered sequence 
	// of applicable methods that was produced by the arguments used in the method call
	// so the args lenght and types must match
	private boolean validArgs(Object[] args) {
		Class<?>[] primaryMethodArgs = primaryMethods.get(0).getParameterTypes();
		return (primaryMethodArgs.length == args.length) &&
				compareTypes(args, primaryMethodArgs);
	}

	//Compares two arrays to verify if the given type parameters are equal
	private boolean compareTypes(Object[] arrayType1, Type[] arrayType2){
		for(int index = 0; index < arrayType1.length; index++)
			if(!arrayType1[index].getClass().getTypeName().equals(arrayType2[index].getTypeName()))
				return false;
		return true;
	}

	private boolean isAroundMethod(GFMethodExtended method) {
		return aroundMethods.contains(method);
	}

	private boolean isPrimaryMethod(GFMethodExtended method) {
		return primaryMethods.contains(method);
	}
	
	private boolean hasAroundMethods() {
		return !aroundMethods.isEmpty();
	}
	
	private boolean hasNextAroundMethod(GFMethodExtended method) {
		return getNextAroundMethod(method) != null;
	}
	
	private boolean hasNextPrimaryMethod(GFMethodExtended method) {
		return getNextPrimaryMethod(method) != null;
	}

	
	
	private GFMethodExtended getNextAroundMethod(GFMethodExtended method) {
		return getNextMethod(aroundMethods, method);
	}

	private GFMethodExtended getNextPrimaryMethod(GFMethodExtended method) {
		return getNextMethod(primaryMethods, method);
	}

	private GFMethodExtended getNextMethod(List<GFMethodExtended> methods, GFMethodExtended method) {
		int index = methods.indexOf(method) + 1;
		if (methods.size() > index)
			return methods.get(index);
		else
			return null;
	}


	
	
	public void setAroundMethods(List<GFMethodExtended> aroundMethods) {
		aroundMethods.stream().forEach(method -> method.setEffectiveMethod(this));
		this.aroundMethods = aroundMethods;
	}
	
	
	public void setBeforeMethods(List<GFMethodExtended> beforeMethods) {
		beforeMethods.stream().forEach(method -> method.setEffectiveMethod(this));
		this.beforeMethods = beforeMethods;
	}

	public void setAfterMethods(List<GFMethodExtended> afterMethods) {
		afterMethods.stream().forEach(method -> method.setEffectiveMethod(this));
		this.afterMethods = afterMethods;
	}

	public void setPrimaryMethod(List<GFMethodExtended> primaryMethods) {
		primaryMethods.stream().forEach(method -> method.setEffectiveMethod(this));
		this.primaryMethods = primaryMethods;
	}
	
	
	
	
	public Object getResponse() {
		return this.response;
	}
}