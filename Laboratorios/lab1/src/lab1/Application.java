package lab1;

import java.util.Scanner;

public class Application {
	
	public static void main(String[] args){
		Scanner inputScanner = new Scanner( System.in );
		String className = "lab1." + inputScanner.nextLine();
		inputScanner.close();
		
		PrintMessage(getClass(className));
	}

	private static void PrintMessage(Class messageClass) {
		try {
			Message message = (Message) messageClass.newInstance();
			message.say();
		} catch (InstantiationException | IllegalAccessException e) {
			System.exit(1);
		}
		
	}

	private static Class getClass(String className) {
		try {
			return Class.forName(className);
		} catch (ClassNotFoundException e) {
			System.err.println("Class '" + className + "' not found");
			System.exit(1);
			return null;
		}
	}
}
