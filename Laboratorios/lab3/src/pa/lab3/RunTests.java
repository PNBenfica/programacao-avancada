package pa.lab3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;

public class RunTests {	

    private static int passed = 0, failed = 0;
    
	public static void main(String[] args) throws Exception {

		String className = args[0];
		HashMap<String, Method> setupMethods = getSetupMethods(className);
		runTests(className, setupMethods);

		System.out.printf("Passed: %d, Failed %d%n", passed, failed);
	}

	// runs all the methods that have the annotation @Test as well
	// as the @setup methods associated
	private static void runTests(String className, HashMap<String, Method> setupMethods) throws SecurityException, ClassNotFoundException {
		invokeSuperClassTests(className, setupMethods);
				
		for (Method m : Class.forName(className).getDeclaredMethods()) {
			if (isTestMethod(m)) {
				try {			
					invokeSetupMethods(m, setupMethods);	
					m.setAccessible(true); // in order to invoke the private methods
					m.invoke(null);
					System.out.printf("Test %s OK! %n", m);
					passed++;
				} catch (Throwable ex) {
					System.out.printf("Test %s failed: %s %n", m, ex.getCause());
					failed++;
				}
			}
		}
	}

	// is a test method if it has @Test annotation, is static, returns void and no parameters
	private static boolean isTestMethod(Method m) {
		return (m.isAnnotationPresent(Test.class)) && (Modifier.isStatic(m.getModifiers())) && (m.getReturnType().equals(Void.TYPE)) && (m.getParameterCount() == 0);
	}

	// invoke the tests of the superclass of className
	private static void invokeSuperClassTests(String className, HashMap<String, Method> setupMethods) throws ClassNotFoundException {

		Class<?> superClass = Class.forName(className).getSuperclass();
		if (superClass != Object.class)
			runTests(superClass.getName(), setupMethods);
	}
	
	// invoke the setup methods of a given method
	private static void invokeSetupMethods(Method method, HashMap<String, Method> setupMethods) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, ClassNotFoundException {

		String[] setupMethodsNames = method.getAnnotation(Test.class).value(); // get the names of the setup methods
		
		if (setupMethodsNames[0].equals("*")) { // must invoke all the setup methods
			setupMethodsNames = setupMethods.keySet().toArray(new String[0]);
		}

		for (String setupMethodName : setupMethodsNames) {
			invokeSetupMethod(setupMethodName, setupMethods);
		}
	}

	// invokes a single setup method. it searches through all the declared methods
	// looking for the one that has the given setup name
	private static void invokeSetupMethod(String setupMethodName, HashMap<String, Method> setupMethods) throws ClassNotFoundException, InvocationTargetException, IllegalAccessException, IllegalArgumentException {		
		
		if (setupMethods.containsKey(setupMethodName)){
			Method setupMethod = setupMethods.get(setupMethodName);
			setupMethod.setAccessible(true);
			setupMethod.invoke(null);
		}		
	}

	
	// returns all the setup methods, from this class and the superclasses
	private static HashMap<String, Method> getSetupMethods(String className) throws ClassNotFoundException {

		HashMap<String, Method> setupMethods = new HashMap<String, Method>();
		for (Method m : Class.forName(className).getDeclaredMethods()) {
			if (m.isAnnotationPresent(Setup.class)) {
				String setupName = m.getAnnotation(Setup.class).value();
				if (!setupMethods.containsKey(setupName)){
					setupMethods.put(setupName, m);
				}
			}
		}
		
		HashMap<String, Method> superClassSetupMethods = getSuperClassSetupMethods(className);
		setupMethods.putAll(superClassSetupMethods);
		return setupMethods;
	}

	private static HashMap<String, Method> getSuperClassSetupMethods(String className) throws ClassNotFoundException {
		Class<?> superClass = Class.forName(className).getSuperclass();
		if (superClass != Object.class)
			return getSetupMethods(superClass.getName());
		return new HashMap<String, Method>();
	}
}
