package pa.lab2;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.Scanner;

public class IntrospectionShell {

	public static void main(String[] args) {
		System.out.println("Introspection Shell v1.125 @2016");
		
		while(true){
			String command = readCommand();
			invokeCommand(command);
		}
	}

	private static String readCommand() {
		String command = "";
		Scanner scanner = new Scanner(System.in);
		while (command.equals("")){
			System.out.print("Command:> ");
			command = scanner.nextLine();
		}
		return command;
	}

	private static void invokeCommand(String commandLine) {
		
		String[] commandTokens = commandLine.split(" ");
		String commandName = commandTokens[0];		
		String[] args = Arrays.copyOfRange(commandTokens, 1, commandTokens.length);
		
		try{
			Command command = getCommand(commandName, args);
			command.execute();
		}
		catch (Exception e){
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	private static Command getCommand(String commandName, String[] args)
			throws InstantiationException, IllegalAccessException, InvocationTargetException {
		
		Command command;
		try{
			Class<?> commandClass = getClass(commandName);
			Constructor<?> constructor = commandClass.getConstructors()[0];
			int argC = constructor.getParameterTypes().length;
			if (argC != args.length)
				throw new IllegalCommandException("Wrong number of arguments");
			command = (Command) constructor.newInstance((Object[]) args);
		}	
		catch (ClassNotFoundException e){
			command = new GenericCommand(commandName, args);
		}
		return command;
	}

	private static Class<?> getClass(String commandName) throws ClassNotFoundException {
		Class<?> requestedCommandClass = Class.forName("pa.lab2." + commandName + "Command");
		return requestedCommandClass;
	}

}
