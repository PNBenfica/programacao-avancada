package pa.lab2;

public class SetCommand  extends Command{

	private String varName;
	
	public SetCommand(String varName){
		this.varName = varName;
	}
	
	@Override
	public void dispatch() {
		Object value = shellStorage.getLastCommandResult();
		if (value != null){
			shellStorage.put(varName, value);
			this.result = value;
			System.out.println("Saved name for object of type: " + result.getClass());
			System.out.println(resultToString(result));
		}
	}

}
