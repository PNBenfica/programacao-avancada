package pa.lab2;

import java.util.TreeMap;

public class ShellStateStorage {

	// Singleton object to record the state of the shell
	private static ShellStateStorage instance = null;
	
	private TreeMap<String, Object> variables = new TreeMap<String, Object>();
	
	private Object lastCommandResult;
	
	public static ShellStateStorage getInstance(){
		if (instance == null)
			instance = new ShellStateStorage();
		return instance;
	}
	
	private ShellStateStorage() {
	}
	
	public void put(String varName, Object obj){
		variables.put(varName, obj);
	}
	
	public Object get(String varName){
		return variables.get(varName);
	}

	public void setLastCommandResult(Object result){
		lastCommandResult = result;
	}
	
	public Object getLastCommandResult(){
		return lastCommandResult;
	}
}
