package pa.lab2;

public abstract class Command {
	
	protected Object result = null;
	protected ShellStateStorage shellStorage = getShellStateStorage();
	
	public void execute(){
		dispatch();
		saveResult();
	}

	public abstract void dispatch();
	
	public ShellStateStorage getShellStateStorage(){
		return ShellStateStorage.getInstance();
	}
	
	private void saveResult() {
		shellStorage.setLastCommandResult(result);
	}
	
	public String resultToString(Object result){
		String message = "";
		if (result.getClass().isArray()){
			for(Object obj: (Object[]) result)
				message += resultToString(obj) + "\n";
		}
		else
			message = result.toString();
		return message;
	}
	
}
