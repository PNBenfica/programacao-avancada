package pa.lab2;

public class ClassCommand extends Command{

	private String className;
	
	public ClassCommand(String className){
		this.className = className;
	}
	
	@Override
	public void dispatch() {
		try{
			Class<?> requestedClass = Class.forName(className);
			this.result = requestedClass;
			System.out.println(requestedClass);
		}
		catch(ClassNotFoundException e){
			System.out.println("Class '" + className + "' not found");
		}
	}

}
