package pa.lab2;

public class GetCommand  extends Command{

	private String varName;
	
	public GetCommand(String varName){
		this.varName = varName;
	}
	
	@Override
	public void dispatch() {
		Object value = shellStorage.get(varName);
		if (value != null){
			this.result = value;
			System.out.println(resultToString(result));
		}
	}

}
