package pa.lab2;

public class IndexCommand  extends Command{

	private int index;
	
	public IndexCommand(String index){
		this.index = Integer.parseInt(index);
	}
	
	@Override
	public void dispatch() {
		Object value = shellStorage.getLastCommandResult();
		if (value != null && value.getClass().isArray()){
			Object[] valueArray = (Object[]) value;
			if (index < valueArray.length){
				this.result = valueArray[index];
				System.out.println(resultToString(result));
			}
		}
		else
			throw new IllegalCommandException("Last result is not an array"); 
	}

}
