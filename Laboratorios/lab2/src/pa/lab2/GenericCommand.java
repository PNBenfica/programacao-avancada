package pa.lab2;

import java.lang.reflect.Method;

public class GenericCommand extends Command{
	
	private String methodName;
	
	private Object[] args;

	public GenericCommand(String methodName, Object[] args){
		this.methodName = methodName;
		this.args = args;
	}
	
	@Override
	public void dispatch() {
		System.out.println("Trying generic command: " + methodName);
		Object lastResult = shellStorage.getLastCommandResult();
		if (lastResult != null){
			
			try {
				Method method = getBestMethod(lastResult);
	            if (methodName.equals("newInstance")) {
	                result = method.invoke(lastResult, new Object[] { args });
	            } else {
	                result = method.invoke(lastResult, args);
	            }
				System.out.println(resultToString(result));
			} catch (Exception e) {
				e.printStackTrace();
				throw new IllegalCommandException("Unnable to call method.");
			}
		}
	}

	private Method getBestMethod(Object lastResult) throws NoSuchMethodException, ClassNotFoundException {
		Method method;
		if (this.args.length > 0){
			Class<?>[] argsTypes = getClasses(this.args);
	        if (methodName.equals("newInstance")) {
	            return lastResult.getClass().getDeclaredMethod(methodName, Object[].class);
	        } else
	        	method = lastResult.getClass().getDeclaredMethod(methodName, argsTypes);
		}
		else{
	        if (methodName.equals("newInstance")) {
	        	method = lastResult.getClass().getDeclaredMethod(methodName, Object[].class);
	        }
	        else
	        	method = lastResult.getClass().getDeclaredMethod(methodName);	
		}
		return method;
	}

	private Class<?>[] getClasses(Object[] array) throws ClassNotFoundException {
		Class<?>[] arrayTypes = new Class[array.length / 2];
		Object[] funArgs = new Object[array.length / 2];
		for(int i = 0; i < array.length; i += 2){
			arrayTypes[i/2] = Class.forName("java.lang." +  array[i]);
			if (arrayTypes[i/2] == Integer.class){
				funArgs[i/2] = (Integer) Integer.parseInt((String) array[i + 1]);
				}
			else if (arrayTypes[i/2] == Double.class){
				funArgs[i/2] = (Double) Double.parseDouble((String) array[i + 1]);
				}
			else{
				funArgs[i/2] = array[i + 1];
			}
		}
		this.args = funArgs;
		return arrayTypes;
	}
	
	// Method to invoke in test
	public Double testMethod(String x, Double y){
		return Double.parseDouble(x) + y;
	}

}
