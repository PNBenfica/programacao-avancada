package pa.lab2;

public class IllegalCommandException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	public IllegalCommandException(String string){
		super(string);
	}

}
