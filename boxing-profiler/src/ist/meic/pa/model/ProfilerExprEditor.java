package ist.meic.pa.model;

import ist.meic.pa.annotations.DoNotInstrument;
import javassist.CannotCompileException;
import javassist.CtClass;
import javassist.Modifier;
import javassist.NotFoundException;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;

@DoNotInstrument
public class ProfilerExprEditor extends ExprEditor {

	// name of the instrumentedMethod
	private String instrumentedMethod;
	
	private static boolean isExtendedVersion;

	public ProfilerExprEditor(String methodName) {
		this.instrumentedMethod = methodName;
	}

	public static void setExtendedVersion(boolean b){
		isExtendedVersion = b;
	}
	
	
	// method called when a method call is found at instrumentedMethod body
	// add instruction to increase counters if method is boxing, unboxing or varArgs.
	public void edit(MethodCall mc) throws CannotCompileException {
		try {
			if (isBoxingMethod(mc)) {
				String template = "{" + "ist.meic.pa.model.ProfilerDataStore.recordBoxing( \"" + instrumentedMethod
						+ "\", $class);" + "$_ = $proceed($$);" + "}";
				mc.replace(template);
			}
			else if (isUnBoxingMethod(mc)){
				String template = "{" + "ist.meic.pa.model.ProfilerDataStore.recordUnboxing( \"" + instrumentedMethod
						+ "\", $class);" + "$_ = $proceed($$);" + "}";
				mc.replace(template);
			}
			else if (isExtendedVersion && isVarargs(mc)){
				String template = "{" + "ist.meic.pa.model.ProfilerDataStore.recordVarargs( \"" + instrumentedMethod
						+ "\");" + "$_ = $proceed($$);" + "}";
				mc.replace(template);
			}

		} catch (NotFoundException e) {
			e.printStackTrace();
		}
	}

	private boolean isBoxingMethod(MethodCall mc) throws NotFoundException {
		String methodLongName = mc.getMethod().getLongName();
		String methodName = mc.getMethod().getName();

		// Integer.valueOf(arg), Long.valueOf(arg), etc. Represent boxing
		if ((methodLongName.startsWith("java.lang.")) && (methodName.equals("valueOf"))) {
			// java.lang.Integer.valueOf() -> Integer
			String wrapperClassName = methodLongName.split("\\.")[2];
			Class<?> wrapperPrimitiveClass = ClassUtils.wrapperToPrimitive("java.lang." + wrapperClassName);
			
			CtClass[] argTypes = mc.getMethod().getParameterTypes();
			// valueOf only has one argument
			String argType = (argTypes.length == 1) ? argTypes[0].getName() : "";
			if (wrapperPrimitiveClass != null)
				return argType.equals(wrapperPrimitiveClass.getName());
		}
		return false;
	}
	
	private boolean isUnBoxingMethod(MethodCall mc) throws NotFoundException{

		String methodLongName = mc.getMethod().getLongName();
		String methodName = mc.getMethod().getName();
		
		// intValue(), longValue(), etc. Represent unboxing
		if ((methodLongName.startsWith("java.lang.")) && (methodName.endsWith("Value"))) {

			// java.lang.Integer.intValue() -> Integer
			String wrapperClassName = methodLongName.split("\\.")[2];
			Class<?> wrapperPrimitiveClass = ClassUtils.wrapperToPrimitive("java.lang." + wrapperClassName);
			// intValue -> int
			String methodPrimitive = methodName.replace("Value","");
			if (wrapperPrimitiveClass != null)
				return ClassUtils.isPrimitive(methodPrimitive) && methodPrimitive.equals(wrapperPrimitiveClass.getName());
		}
		
		return false;
	}

	private boolean isVarargs(MethodCall mc) throws NotFoundException{
		return (mc.getMethod().getModifiers() & Modifier.VARARGS) != 0;
	}	

}
