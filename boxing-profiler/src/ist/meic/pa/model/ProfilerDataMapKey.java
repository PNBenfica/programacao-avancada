package ist.meic.pa.model;

import ist.meic.pa.annotations.DoNotInstrument;

// Class used as key in the Map that records the profiling results for each method
@DoNotInstrument
public class ProfilerDataMapKey implements Comparable<ProfilerDataMapKey> {

	// method that has autoboxing
	private String methodName;

	// boxing or unboxing
	private String autoboxingType;

	// class of the entity that was autoboxed
	private String autoboxingClassName;

	public ProfilerDataMapKey(String methodName, String autoboxingType, String autoboxingClassName) {
		this.methodName = methodName;
		this.autoboxingType = autoboxingType;
		this.autoboxingClassName = autoboxingClassName;
	}

	public String getMethodName() {
		return this.methodName;
	}

	public String getAutoboxingType() {
		return this.autoboxingType;
	}

	public String getAutoboxingClassName() {
		return this.autoboxingClassName;
	}

	@Override
	public int hashCode() {
		return methodName.hashCode() + autoboxingType.hashCode() + autoboxingClassName.hashCode();
	}
	
	public int compareName(ProfilerDataMapKey obj){
		return this.methodName.compareTo(obj.getMethodName());
	}
	
	public int compareClass(ProfilerDataMapKey obj){
		return this.autoboxingClassName.compareTo(obj.getAutoboxingClassName());
	}
	
	public int compareType(ProfilerDataMapKey obj){
		return this.getAutoboxingType().compareTo(obj.getAutoboxingType());
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof ProfilerDataMapKey))
			return false;
		if (obj == this)
			return true;

		ProfilerDataMapKey data = (ProfilerDataMapKey) obj;
		return this.getMethodName().equals(data.getMethodName())
				&& this.getAutoboxingType().equals(data.getAutoboxingType())
				&& this.getAutoboxingClassName().equals(data.getAutoboxingClassName());
	}

	@Override
	public int compareTo(ProfilerDataMapKey key) {
		int longName = this.compareName(key);
		if(longName == 0){
			int nameClass = this.compareClass(key);
			if(nameClass == 0)
				return this.compareType(key);
			else
				return nameClass;
		}
		return longName;
	}

}
