package ist.meic.pa.model;

import ist.meic.pa.annotations.DoNotInstrument;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;
import javassist.Translator;

public class ProfilerTranslator implements Translator {
	
	// called before class is loaded, used to instrument class methods 
	@Override
	public void onLoad(ClassPool pool, String className) throws NotFoundException, CannotCompileException {

		CtClass ctClass = pool.get(className);
		
		if (ctClass.hasAnnotation(DoNotInstrument.class) || ctClass.getName().startsWith("javassist."))
			return;
		
		for (final CtMethod ctMethod : ctClass.getDeclaredMethods()) {
			instrumentMethod(ctMethod);
		}

	}

	private void instrumentMethod(final CtMethod ctMethod) throws CannotCompileException {
		String methodLongName = ctMethod.getLongName();
		String methodName = ctMethod.getName();
		
		if (methodName.equals("main")){
			ctMethod.insertAfter("ist.meic.pa.model.ProfilerDataStore.printResults();");
		}			
		
		ctMethod.instrument(new ProfilerExprEditor(methodLongName));
	}

	// called when this event listener is added to a class loader
	// nothing to do
	@Override
	public void start(ClassPool arg0) throws NotFoundException, CannotCompileException {
	}

}
