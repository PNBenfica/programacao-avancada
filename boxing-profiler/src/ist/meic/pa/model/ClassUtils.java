package ist.meic.pa.model;

import java.util.HashMap;
import java.util.Map;

import ist.meic.pa.annotations.DoNotInstrument;

//class that maps primitive types with correspondent wrapper class
@DoNotInstrument
public class ClassUtils {

	//key -> type | value -> class
	private static final Map<Class<?>, Class<?>> primitiveWrapperMap = new HashMap<Class<?>, Class<?>>();

	static {
		primitiveWrapperMap.put(Boolean.TYPE, Boolean.class);
		primitiveWrapperMap.put(Byte.TYPE, Byte.class);
		primitiveWrapperMap.put(Character.TYPE, Character.class);
		primitiveWrapperMap.put(Short.TYPE, Short.class);
		primitiveWrapperMap.put(Integer.TYPE, Integer.class);
		primitiveWrapperMap.put(Long.TYPE, Long.class);
		primitiveWrapperMap.put(Double.TYPE, Double.class);
		primitiveWrapperMap.put(Float.TYPE, Float.class);
		primitiveWrapperMap.put(Void.TYPE, Void.TYPE);
	}

	//key -> class | value -> type
	private static final Map<Class<?>, Class<?>> wrapperPrimitiveMap = new HashMap<Class<?>, Class<?>>();

	static {
		for (final Map.Entry<Class<?>, Class<?>> entry : primitiveWrapperMap.entrySet()) {
			final Class<?> primitiveClass = entry.getKey();
			final Class<?> wrapperClass = entry.getValue();
			if (!primitiveClass.equals(wrapperClass)) {
				wrapperPrimitiveMap.put(wrapperClass, primitiveClass);
			}
		}
	}

	public static boolean isPrimitiveWrapper(final Class<?> type) {
		return wrapperPrimitiveMap.containsKey(type);
	}

	public static Class<?> wrapperToPrimitive(final Class<?> cls) {
		return wrapperPrimitiveMap.get(cls);
	}

	public static Class<?> wrapperToPrimitive(final String clsName) {
		Class<?> wrapperClass = getClass(clsName);
		return wrapperPrimitiveMap.get(wrapperClass);
	}

	public static Class<?> getClass(String className) {
		try {
			return Class.forName(className);
		} catch (ClassNotFoundException e) {
			return null;
		}
	}
	
	public static boolean isPrimitive(String str){
		for (Class<?> key: primitiveWrapperMap.keySet()){
			if(key.getName().equals(str))
				return true;
		}
		return false;
	}
	
}
