package ist.meic.pa.model;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import ist.meic.pa.annotations.DoNotInstrument;

// class that stores the number of boxing, unboxing and varArgs(extended version only)
@DoNotInstrument
public class ProfilerDataStore {
	
	// key -> (methodName, autoboxingType, autoboxingClassName) | value -> number of occurrences 
	private static Map<ProfilerDataMapKey, Integer> profilerData = new TreeMap<ProfilerDataMapKey, Integer>();

	
	public static void recordBoxing(String methodName, Class<?> autoboxingClass){
		store(methodName, "boxed", autoboxingClass.getName());
	}	
	
	public static void recordUnboxing(String methodName, Class<?> autoboxingClass){
		store(methodName, "unboxed", autoboxingClass.getName());
	}

	public static void recordVarargs(String methodName){
		store(methodName, "varargs", "");
	}
	
	private static void store(String methodName, String conversionType, String autoboxingClassName) {
		ProfilerDataMapKey key = new ProfilerDataMapKey(methodName, conversionType, autoboxingClassName);
		if (profilerData.containsKey(key)){
			int value = profilerData.get(key) + 1;
			profilerData.put(key, value);
		}
		else{
			profilerData.put(key, 1);
		}
	}
	
	public static void printResults(){
		for (Entry<ProfilerDataMapKey, Integer> entry: profilerData.entrySet()){
			ProfilerDataMapKey key = (ProfilerDataMapKey) entry.getKey();
			Integer value = (Integer) entry.getValue();
			System.err.println(key.getMethodName() + " " + key.getAutoboxingType() + " " + value + " " + key.getAutoboxingClassName());
		}
	}	
}
