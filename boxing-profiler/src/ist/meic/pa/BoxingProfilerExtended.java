package ist.meic.pa;

import ist.meic.pa.annotations.DoNotInstrument;
import ist.meic.pa.model.ProfilerExprEditor;
import ist.meic.pa.model.ProfilerTranslator;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.Loader;
import javassist.NotFoundException;
import javassist.Translator;

@DoNotInstrument
public class BoxingProfilerExtended {

	public static void main(String[] args) throws ClassNotFoundException {
		if (args.length < 1) {
			System.err.println("Invalid argument(s)");
            System.err.println("Usage: java ist.meic.pa.BoxingProfilerExtended <CLASS_NAME>");
            return;
		} 
		else {	
			try {
				Translator translator = new ProfilerTranslator();
				ProfilerExprEditor.setExtendedVersion(true);
				ClassPool pool = ClassPool.getDefault();
				Loader classLoader = new Loader();
				classLoader.addTranslator(pool, translator);
				classLoader.run(args[0], new String[]{});
			} catch (NotFoundException | CannotCompileException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
}
