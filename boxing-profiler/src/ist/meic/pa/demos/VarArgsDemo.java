package ist.meic.pa.demos;

public class VarArgsDemo {
	
	public static void main(String[] args){
		method("ola", "hhmvh", 5);
		method("ola", "hhmvh", 5);
		johnOla();
	}

	private static void method(Object...args) {
		for (Object arg: args)
			System.out.println(arg);
		System.out.printf("%s %s %n", "ola", "mundo");
	}

	private static void johnOla(Object...args) {
		for (int i = 0; i < 1000000; i++)
			String.format("olas%s%s", "ola", "oi");
	}
}
